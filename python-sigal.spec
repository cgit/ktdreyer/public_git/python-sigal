%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print get_python_lib()")}

%if 0%{?fedora}
%global with_python3 1
%{!?python3_version: %global python3_version %(%{__python3} -c "import sys; sys.stdout.write(sys.version[:3])")}
%endif

%define srcname sigal

Name:           python-sigal
Version:        0.8.1
Release:        2%{dist}
Summary:        Simple static gallery generator
Group:          Development/Libraries
License:        BSD
URL:            https://github.com/saimn/sigal
Source0:        http://pypi.python.org/packages/source/s/%{srcname}/%{srcname}-%{version}.tar.gz
Requires:       python-blinker
Requires:       python-click
Requires:       python-jinja2
Requires:       python-markdown
Requires:       python-pillow
Requires:       python-pilkit
BuildRequires:  pytest
BuildRequires:  python-devel >= 2.6
BuildRequires:  python-blinker
BuildRequires:  python-click
BuildRequires:  python-jinja2
BuildRequires:  python-markdown
BuildRequires:  python-pillow
BuildRequires:  python-pilkit
%if 0%{?with_python3}
BuildRequires:  python3-devel >= 3.1
BuildRequires:  python3-pytest
BuildRequires:  python3-blinker
BuildRequires:  python3-click
BuildRequires:  python3-jinja2
BuildRequires:  python3-markdown
BuildRequires:  python3-pillow
BuildRequires:  python3-pilkit
%endif # with_python3
BuildArch:      noarch

%description
Simple static gallery generator.

%if 0%{?with_python3}
%package -n python3-sigal
Summary:        Utilities and processors for the Python Imaging Libary.
Group:          Development/Languages
Requires:       python3 >= 3.1

%description -n python3-sigal
Simple static gallery generator.
%endif # with_python3

%prep
%setup -q -n %{srcname}-%{version}

# remove shebangs
find sigal -type f -name '*.py' \
  -exec sed -i -e '/^#!/{1D}' {} \;

# fix line-ending
find docs -type f \
  -exec sed -i 's/\r//' {} \;

# loosen nose deps
sed -i setup.py \
  -e 's/nose==1.2.1/nose/' \
  -e 's/nose-progressive==1.3/nose-progressive/'

%if 0%{?with_python3}
cp -a . %{py3dir}
%endif # with_python3

%build
%{__python} setup.py build

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py build
popd
%endif # with_python3

%install
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# rename binary
mv %{buildroot}%{_bindir}/sigal{,-%{python_version}}

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py install -O1 --skip-build --root %{buildroot}
popd

# rename binary
mv %{buildroot}%{_bindir}/sigal{,-%{python3_version}}
%endif # with_python3

# 2.X binary is called by default for now
ln -s sigal-%{python_version} %{buildroot}%{_bindir}/sigal

%check
%{__python2} setup.py test

%if 0%{?with_python3}
pushd %{py3dir}
%{__python3} setup.py test
popd
%endif # with_python3

%files
%doc docs/*
%{python_sitelib}/*
%{_bindir}/sigal
%{_bindir}/sigal-%{python_version}

%if 0%{?with_python3}
%files -n python3-sigal
%doc docs/*
%{python3_sitelib}/*
%{_bindir}/sigal-%{python3_version}
%endif # with_python3

%changelog
* Tue Nov 11 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.8.1-2
- Fix python3 subpackage description
- Fix older changelog entry's version

* Mon Oct 27 2014 Ken Dreyer <ktdreyer@ktdreyer.com> - 0.8.1-1
- New package.
